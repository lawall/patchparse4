(* IMPORTANT! 
 * An expression cannot follow another expression.  
 * A symbol cannot follow another symbol. 
 *)
(* not fault tolerant *)

%{
open Parse_error
open Ast0

let parse_error _ =
  display_error "Syntax error"
    (Parsing.symbol_start ()) (Parsing.symbol_end ())

let mkinfo (s,(ln,ty,tag)) = (s,(ln,ty))
let mkbinfo (ln,ty,tag)    = (ln,ty)

(* This does not work with symbols as mkinfo deletes the tag
let mkcall fn arg known =
  let atfront (_,(_,_,tag)) = tag = FRONT in
  if atfront fn
  then DECLARER(mkinfo fn,arg,known)
  else CALL(mkinfo fn,arg,known)
*)
%}

%token <string * (int * Patch.linetype * Parse_error.atfront)> IDENT
%token <string * (int * Patch.linetype * Parse_error.atfront)> CST_CHAR
%token <string * (int * Patch.linetype * Parse_error.atfront)> CST_INT
%token <string * (int * Patch.linetype * Parse_error.atfront)> CST_STRING
%token <string * (int * Patch.linetype * Parse_error.atfront)> STERM
%token <string * (int * Patch.linetype * Parse_error.atfront)> ESEP
%token <string * (int * Patch.linetype * Parse_error.atfront)> OPERATOR
%token <string * (int * Patch.linetype * Parse_error.atfront)> EQ
%token <string * (int * Patch.linetype * Parse_error.atfront)> SYMOP
%token <string * (int * Patch.linetype * Parse_error.atfront)> DEREFOP
%token <string * (int * Patch.linetype * Parse_error.atfront)> TYPE
%token <string * (int * Patch.linetype * Parse_error.atfront)> PRIM
%token <string * (int * Patch.linetype * Parse_error.atfront)> INCLUDE

%token EOF 
%token <int * Patch.linetype * Parse_error.atfront>
  LPAREN "(" RPAREN ")" LBRACE "{" RBRACE "}" LBRACK "[" RBRACK "]" DEFINE

(* Non-terminals informations *)
%start interpret
%type <Ast0.code list> interpret

%%

interpret:
  | toplevel EOF {$1}
  | EOF {[]}

toplevel:
  | INCLUDE          {[EXPR([SYMBOL([IDENT(mkinfo $1)])])]}
  | INCLUDE toplevel {EXPR([SYMBOL([IDENT(mkinfo $1)])])::$2}

  | DEFINE {[SEP("#define", mkbinfo $1)]}

  | sterm          {[SEP($1)]}
  | sterm toplevel {SEP($1)::$2}

  | expression sterm          {[EXPR($1); SEP($2)]}
  | expression sterm toplevel {EXPR($1)::(SEP($2)::$3)}

  | sep                      {[SEP($1)]}
  | sep toplevel             {SEP($1)::$2}
  | expression sep           {[EXPR($1); SEP($2)]}
  | expression sep toplevel  {EXPR($1)::(SEP($2)::$3)}

  | expression    {[EXPR($1)]}
  | expressionend {[EXPR($1)]}

  | expression expressionrhs sterm toplevel {[EXPR($1 @ $2); SEP($3)] @ $4}
  | expression expressionrhs sterm          {[EXPR($1 @ $2); SEP($3)]}
  | expression expressionrhs sep toplevel   {[EXPR($1 @ $2); SEP($3)] @ $4}
  | expression expressionrhs sep            {[EXPR($1 @ $2); SEP($3)]}
  | expression expressionrhs                {[EXPR($1 @ $2)]}
  | expression expressionrhsend             {[EXPR($1 @ $2)]}

  | expressionrhs sterm toplevel {[EXPR($1); SEP($2)] @ $3}


statements:
  | expression expressionrhs STERM statements {[EXPR($1 @ $2); SEP(mkinfo $3)] @ $4}
  | expression expressionrhs STERM            {[EXPR($1 @ $2); SEP(mkinfo $3)]}

  | expression STERM statements {EXPR($1)::(SEP(mkinfo $2)::$3)}
  | expression STERM            {[EXPR($1); SEP(mkinfo $2)]}

  | expression LBRACE statements RBRACE            {EXPR($1)::SEP("{", mkbinfo $2)::($3 @ [SEP("{", mkbinfo $4)])}
  | expression LBRACE statements RBRACE statements {EXPR($1)::SEP("{", mkbinfo $2)::($3 @ [SEP("{", mkbinfo $4)] @ $5)}

statementsend:
  | expression expressionrhs STERM statementsend {[EXPR($1 @ $2); SEP(mkinfo $3)] @ $4}
  | expression STERM statementsend               {EXPR($1)::(SEP(mkinfo $2)::$3)}

  | expression LBRACE statements RBRACE statementsend {EXPR($1)::SEP("{", mkbinfo $2)::($3 @ [SEP("{", mkbinfo $4)] @ $5)}
  
  | expression LBRACE statements {EXPR($1)::SEP("{", mkbinfo $2)::$3}


expressionrhs:
  | EQ expression {[ASSIGN(mkinfo $1, $2, Ast.KNOWN)]}
  | EQ structure  {[ASSIGN(mkinfo $1, [$2], Ast.KNOWN)]}

  | EQ expression expressionrhs {[ASSIGN(mkinfo $1, $2 @ $3, Ast.KNOWN)]}

expressionrhsend:
  | EQ expressionend {[ASSIGN(mkinfo $1, $2, Ast.ENDUNKNOWN)]}
  | EQ structureend  {[ASSIGN(mkinfo $1, [$2], Ast.ENDUNKNOWN)]}
  | EQ               {[ASSIGN(mkinfo $1, [], Ast.ENDUNKNOWN)]}

  | EQ expression expressionrhsend {[ASSIGN(mkinfo $1, $2 @ $3, Ast.ENDUNKNOWN)]}


/* subset of expressions that can follow a symbol */
expraftersymbol:
  | OPERATOR expression {EOP(mkinfo $1)::$2}
  | DEREFOP expression  {DSYMBOL([DEREFOP(mkinfo $1)])::$2}

  | OPERATOR {[EOP(mkinfo $1)]}
  | DEREFOP  {[DSYMBOL([DEREFOP(mkinfo $1)])]}

expraftersymbolend:
  | OPERATOR expressionend {EOP(mkinfo $1)::$2}
  | DEREFOP expressionend  {DSYMBOL([DEREFOP(mkinfo $1)])::$2}


expression:
  | expraftersymbol {$1}

  | symbol {[SYMBOL($1)]}

  | symbol expraftersymbol {SYMBOL($1)::$2}

  | expressionparen            {$1}
  | expressionparen expression {$1 @ $2}

  | symbol "(" ")"                 {[CALL($1,[],Ast.KNOWN)]}
  | symbol "(" args ")"            {[CALL($1,$3,Ast.KNOWN)]}
  | symbol "(" ")" expression      {CALL($1,[],Ast.KNOWN)::$4}
  | symbol "(" args ")" expression {CALL($1,$3,Ast.KNOWN)::$5}

expressionend:
  | expraftersymbolend {$1}

  | symbolend {[SYMBOL($1)]}
  | symbol expraftersymbolend {SYMBOL($1)::$2}

  | expressionparen expressionend        {$1 @ $2}
  | expressionparenend                   {$1}

  | symbol "("                        {[CALL($1,[],Ast.ENDUNKNOWN)]}
  | symbol "(" args                   {[CALL($1,$3,Ast.ENDUNKNOWN)]}
  | symbol "(" argsend                {[CALL($1,$3,Ast.ENDUNKNOWN)]}
  | symbol "(" ")" expressionend      {CALL($1,[],Ast.KNOWN)::$4}
  | symbol "(" args ")" expressionend {CALL($1,$3,Ast.KNOWN)::$5}


expressionparen:
  | "(" args ")" {[PAREN($2, Ast.KNOWN)]}
  | "(" ")"      {[PAREN([], Ast.KNOWN)]}

expressionparenend:
  | "("         {[PAREN([], Ast.ENDUNKNOWN)]}
  | "(" args    {[PAREN($2, Ast.ENDUNKNOWN)]}
  | "(" argsend {[PAREN($2, Ast.ENDUNKNOWN)]}


structargs: /* struct or array body */
  | arg                {[EXPR($1)]}
  | arg sep            {[EXPR($1); SEP($2)]}
  | arg sep structargs {EXPR($1)::SEP($2)::$3}
  | sep structargs     {SEP($1)::$2}
  | sep                {[SEP($1)]}

structargsend:
  | argend                {[EXPR($1)]}
  | arg sep structargsend {EXPR($1)::SEP($2)::$3}


args:
  | arg             {[EXPR($1)]}
  | arg sep         {[EXPR($1); SEP($2)]}
  | arg sep args    {EXPR($1)::SEP($2)::$3}
  | arg forsep      {[EXPR($1); SEP($2)]}
  | arg forsep args {EXPR($1)::SEP($2)::$3}

  | sep         {[SEP($1)]}
  | sep args    {SEP($1)::$2}
  | forsep      {[SEP($1)]}
  | forsep args {SEP($1)::$2}

argsend:
  | argend             {[EXPR($1)]}
  | arg sep argsend    {EXPR($1)::SEP($2)::$3}
  | arg forsep argsend {EXPR($1)::SEP($2)::$3}


arg:
  | expression {$1}
  | structure  {[$1]}

  | expression structure {[$2]}

  | EQ {[SYMBOL([IDENT(mkinfo $1)])]} /* special case for example: ATOMIC_OPS(and, &=, and) */

  | LBRACE statements RBRACE {[STRUCT(SEP("{", mkbinfo $1)::($2 @ [SEP("{", mkbinfo $3)]), Ast.KNOWN)]}

  | expression expressionrhs {$1 @ $2}

  | expression EQ expressionparen structure {$1 @ [ASSIGN(mkinfo $2, [$4], Ast.KNOWN)]}
  | expression EQ DEREFOP expressionparen structure {$1 @ [ASSIGN(mkinfo $2, [$5], Ast.KNOWN)]}

argend:
  | structureend  {[$1]}
  | expressionend {$1}

  | expression expressionrhsend {$1 @ $2}

  | LBRACE statements    {[STRUCT(SEP("{", mkbinfo $1)::$2, Ast.ENDUNKNOWN)]}
  | LBRACE statementsend {[STRUCT(SEP("{", mkbinfo $1)::$2, Ast.ENDUNKNOWN)]}

  | expression EQ expressionparen structureend {$1 @[ASSIGN(mkinfo $2, [$4], Ast.ENDUNKNOWN)]}
  | expression EQ DEREFOP expressionparen structureend {$1 @[ASSIGN(mkinfo $2, [$5], Ast.ENDUNKNOWN)]}

  | expression structureend {[$2]}

symbol:
  | atoken symbol             {$1::$2}
  | atoken                    {[$1]}
  | "[" expression "]" symbol {(ARRAY($2, Ast.KNOWN)::$4)}
  | "[" expression "]"        {[ARRAY($2, Ast.KNOWN)]}
  | "[" "]" symbol            {ARRAY([], Ast.KNOWN)::$3}
  | "[" "]"                   {[ARRAY([], Ast.KNOWN)]}

  | "[" expression expressionrhs "]" {[ARRAY($2, Ast.KNOWN)]} /* should support? */

symbolend:
  | atoken symbolend             {$1::$2}
  | "[" expression "]" symbolend {(ARRAY($2, Ast.KNOWN)::$4)}
  | "[" expression               {[ARRAY($2, Ast.ENDUNKNOWN)]}
  | "[" expressionend            {[ARRAY($2, Ast.ENDUNKNOWN)]}
  | "[" "]" symbolend            {ARRAY([], Ast.KNOWN)::$3}
  | "["                          {[ARRAY([], Ast.ENDUNKNOWN)]}

atoken:
  | IDENT       {IDENT(mkinfo $1)}
  | CST_CHAR    {CHAR(mkinfo $1)}
  | CST_INT     {INT(mkinfo $1)}
  | CST_STRING  {STR(mkinfo $1)}
  | SYMOP       {SYMOP(mkinfo $1)}
  | TYPE        {TYPE(mkinfo $1)}
  | PRIM        {IDENT(mkinfo $1)}

sep:
  | ESEP {mkinfo $1}

forsep:
  | STERM {mkinfo $1}

sterm:
  | STERM   {mkinfo $1}
  | "{"     {"{", mkbinfo $1}
  | "}"     {"}", mkbinfo $1}
  | ")"     {")", mkbinfo $1}
  | "]"     {"]", mkbinfo $1}

structure:
  | "{" structargs "}"       {STRUCT($2, Ast.KNOWN)}
  | "{" "}"            {STRUCT([], Ast.KNOWN)}

structureend:
  | "{" structargs    {STRUCT($2, Ast.ENDUNKNOWN)}
  | "{" structargsend {STRUCT($2, Ast.ENDUNKNOWN)}
  | "{"               {STRUCT([], Ast.ENDUNKNOWN)}
