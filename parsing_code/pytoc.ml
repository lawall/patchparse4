let is_alpha = function 'a' .. 'z' | 'A' .. 'Z' -> true | _ -> false

let is_digit = function '0' .. '9' -> true | _ -> false

let is_alnum ch = is_alpha ch || is_digit ch

let pytoc s =
  let includes_import line =
    try
      ignore (Str.search_forward (Str.regexp "import") line 0);
      true
    with Not_found -> false
  in

  let instring_single = ref false in
  let instring_double = ref false in
  let paren_stack = ref [] in
  let escape = ref false in

  s |> String.split_on_char '\n'
  |> List.map (fun line ->
         escape := false;

         let line_number_regex = Str.regexp "___line=[0-9]+" in
         try
           let keyword_line = ref false in

           (* Some lines seem to not have line numbers *)
           ignore (Str.search_forward line_number_regex line 0);
           let line_num_str = Str.matched_string line in
           let line = Str.global_replace line_number_regex "" line in
           let line = String.trim line in
           if String.length line == 0 then line
           else if includes_import line then ""
           else
             let rec convert_line seq =
               match seq with
               | [] -> []
               (* comment *)
               | '#' :: rest when not (!instring_single || !instring_double) ->
                   []
               (* string *)
               | '"' :: rest when not (!escape || !instring_single) ->
                   instring_double := not !instring_double;
                   '"'
                   ::
                   (match rest with
                   | ':' :: rest2 when not !keyword_line ->
                       '=' :: convert_line rest2
                   | _ -> convert_line rest)
               | '"' :: rest when !instring_single && not !escape ->
                   (* We are inside a single quote string which is being
                      converted to double quote, so escape double quotes*)
                   instring_double := not !instring_double;
                   '\\' :: '"' :: convert_line rest
               (* string *)
               | '\'' :: rest when not (!escape || !instring_double) ->
                   instring_single := not !instring_single;
                   '"'
                   ::
                   (match rest with
                   | ':' :: rest2 when not !keyword_line ->
                       '=' :: convert_line rest2
                   | _ -> convert_line rest)
               (* if *)
               | ch :: 'i' :: 'f' :: ' ' :: rest when not (is_alnum ch) ->
                   keyword_line := true;
                   paren_stack := '(' :: !paren_stack;
                   [ ch; 'i'; 'f'; ' '; '(' ] @ convert_line rest
               (* for *)
               | ch :: 'f' :: 'o' :: 'r' :: ' ' :: rest when not (is_alnum ch)
                 ->
                   keyword_line := true;
                   paren_stack := '(' :: !paren_stack;
                   [ ch; 'f'; 'o'; 'r'; ' '; '(' ] @ convert_line rest
               (* while *)
               | ch :: 'w' :: 'h' :: 'i' :: 'l' :: 'e' :: ' ' :: rest
                 when not (is_alnum ch) ->
                   keyword_line := true;
                   paren_stack := '(' :: !paren_stack;
                   [ ch; 'w'; 'h'; 'i'; 'l'; 'e'; ' '; '(' ] @ convert_line rest
               (* elif *)
               | ch :: 'e' :: 'l' :: 'i' :: 'f' :: ' ' :: rest
                 when not (is_alnum ch) ->
                   keyword_line := true;
                   paren_stack := '(' :: !paren_stack;
                   [ ch; 'e'; 'l'; 's'; 'e'; ' '; 'i'; 'f'; ' '; '(' ]
                   @ convert_line rest
               | [ ':' ] -> (
                   match !paren_stack with
                   | '(' :: paren_rest ->
                       paren_stack := paren_rest;
                       [ ')'; ';' ]
                   | _ -> [ ';' ])
               | [ '}' ] -> (
                   match !paren_stack with
                   | '(' :: paren_rest ->
                       paren_stack := paren_rest;
                       [ ')'; '}'; ';' ]
                   | _ -> [ '}'; ';' ])
               (* array *)
               | ch :: '[' :: rest when not (is_alnum ch) ->
                   paren_stack := '{' :: !paren_stack;
                   ch :: '{' :: convert_line rest
               (* index *)
               | ch :: '[' :: rest when is_alnum ch ->
                   paren_stack := '[' :: !paren_stack;
                   ch :: '[' :: convert_line rest
               (* array *)
               | '=' :: ' ' :: '[' :: rest ->
                   paren_stack := '{' :: !paren_stack;
                   '=' :: ' ' :: '{' :: convert_line rest
               | [ ']' ] -> (
                   match !paren_stack with
                   | '{' :: paren_rest ->
                       paren_stack := paren_rest;
                       [ '}'; ';' ]
                   | '[' :: paren_rest ->
                       paren_stack := paren_rest;
                       [ ']'; ';' ]
                   | '(' :: paren_rest ->
                       paren_stack := paren_rest;
                       ')'
                       ::
                       (match !paren_stack with
                       | '{' :: paren_rest ->
                           paren_stack := paren_rest;
                           [ '}'; ';' ]
                       | '[' :: paren_rest ->
                           paren_stack := paren_rest;
                           [ ']'; ';' ]
                       | _ -> (* Should not happen*) [])
                   | _ -> (* Should not happen*) [])
               | ']' :: rest -> (
                   match !paren_stack with
                   | '{' :: paren_rest ->
                       paren_stack := paren_rest;
                       '}' :: convert_line rest
                   | '[' :: paren_rest ->
                       paren_stack := paren_rest;
                       ']' :: convert_line rest
                   | '(' :: paren_rest ->
                       paren_stack := paren_rest;
                       ')'
                       ::
                       (match !paren_stack with
                       | '{' :: paren_rest ->
                           paren_stack := paren_rest;
                           [ '}'; ';' ]
                       | '[' :: paren_rest ->
                           paren_stack := paren_rest;
                           [ ']'; ';' ]
                       | _ -> (* Should not happen*) [])
                       @ convert_line rest
                   | _ -> (* Should not happen*) [])
               | [ ch ] -> (
                   match ch with
                   | '%' | ',' | '[' | '{' | '(' -> [ ch ]
                   | _ ->
                       escape := false;
                       [ ch; ';' ])
               | ch :: rest ->
                   escape := false;
                   ch :: convert_line rest
             in

             (line |> String.to_seq |> List.of_seq |> convert_line
            |> List.to_seq |> String.of_seq)
             ^ line_num_str
         with Not_found -> (* we return them as is to avoid crashing *) line)
  |> String.concat "\n"
